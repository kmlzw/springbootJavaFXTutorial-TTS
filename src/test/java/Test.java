import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Future;
import com.microsoft.cognitiveservices.speech.*;
import com.microsoft.cognitiveservices.speech.audio.AudioConfig;
import com.microsoft.cognitiveservices.speech.translation.SpeechTranslationConfig;

public class Test {
    /**
     * @param args Arguments are ignored in this sample.
     */

    public static void main(String[] args) {
        try {
            String speechSubscriptionKey = "bd6fe577317c4f1dba23b9176228c0ab";
            String serviceRegion = "eastasia";
            String lang = "zh-CN";

            int exitCode = 1;
            SpeechConfig speechConfig = SpeechConfig.fromSubscription(speechSubscriptionKey, serviceRegion);
            speechConfig.setSpeechSynthesisLanguage(lang);


            assert(speechConfig != null);

            AudioConfig audioConfig = AudioConfig.fromWavFileOutput("D://temp//speech_ + "+ new Date().getTime() +".wav");

            SpeechSynthesizer synth = new SpeechSynthesizer(speechConfig,audioConfig);

            assert(synth != null);


            String text = "你好，李焕英是一部好电影，值得一看！";

            Future<SpeechSynthesisResult> task = synth.SpeakTextAsync(text);
            assert(task != null);

            SpeechSynthesisResult result = task.get();
            assert(result != null);



            if (result.getReason() == ResultReason.SynthesizingAudioCompleted) {
                System.out.println("Speech synthesized to speaker for text [" + text + "]");
                exitCode = 0;
            }
            else if (result.getReason() == ResultReason.Canceled) {
                SpeechSynthesisCancellationDetails cancellation = SpeechSynthesisCancellationDetails.fromResult(result);
                System.out.println("CANCELED: Reason=" + cancellation.getReason());

                if (cancellation.getReason() == CancellationReason.Error) {
                    System.out.println("CANCELED: ErrorCode=" + cancellation.getErrorCode());
                    System.out.println("CANCELED: ErrorDetails=" + cancellation.getErrorDetails());
                    System.out.println("CANCELED: Did you update the subscription info?");
                }
            }

            result.close();
            synth.close();

            System.exit(exitCode);
        } catch (Exception ex) {
            System.out.println("Unexpected exception: " + ex.getMessage());

            assert(false);
            System.exit(1);
        }
    }

}
