package com.mvp.java.controllers;

import com.mvp.java.services.TextToSpeechService;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class LoggerTabController {

    @FXML private TextArea loggerTxtArea;

    @Autowired
    private TextToSpeechService ttsService;

    public TextArea getLoggerTxtArea() {
        return loggerTxtArea;
    }

    @FXML
    private void clear(){
        this.getLoggerTxtArea().clear();
    }

    @FXML
    private void output(){
        String text = this.getLoggerTxtArea().getText();
        System.out.println("11111111111111lzw:" + text);
        Map<String,Object> resultMap = this.ttsService.textToSpeech(text);
        this.getLoggerTxtArea().setText("工作完成: " + resultMap.get("exitCode") + "," + resultMap.get("exitMsg"));
    }
    
}
